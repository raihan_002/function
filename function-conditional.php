<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>berlatih function php</h1>
<?php
 
 echo "<h3> Soal No 1 Greetings </h3>";

 function greetings($nama){
    echo "Halo " . $nama . ", Selamat datang <br>";
 }

 greetings("Bagas");
 greetings("Wahyu");
 greetings("Abdul");

 echo "<br>";

 echo "<br>";

 echo "<h3>Soal No 2 Reverse String</h3>";


 function reverse($kata1){
    $panjangkata = strlen($kata1);
    $tampung = "";
    for ($i=($panjangkata - 1); $i>=0; $i--){
        $tampung .= $kata1[$i];
    }
    return $tampung;
}
function reverseString($kata2){
    $string=reverse($kata2);
    echo $string . "<br>";
}

reverseString("nama mahasiswa");
reverseString("AMIK Selatpanjang");
reverseString("We Are AMIK Developers");
reverseString("ini katak");
reverseString("uvuvwewevwe onyentenyevwevwe ugwemubwem osas");
echo "<br>";

echo "<h3>Soal No 3 Palindrome </h3>";


function palindrome($pali){
    $balikKata = reverse($pali);
    if($balikKata === $pali){
        echo "True <br>";
    }
    else {
        echo "False <br>";
    }
}

palindrome("civic") ; // true
palindrome("nababan") ; // true
palindrome("jambaban"); // false
palindrome("racecar"); // true

echo "<h3>Soal No 4 Tentukan Nilai </h3>";

function tentukan_nilai($angka){
    $output = "";
    if($angka>=98 && $angka <=100){
        $output .= "Sangat Baik";
    }else if ($angka>=76 && $angka <98){
        $output .= "Baik";
    }else if ($angka>=67 && $angka <76){
        $output .= "Cukup";
    }else if ($angka>=43 && $angka <67){
        $output .= "Kurang";
    }
    return $output . "<br>";

}

echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>

</body>

</html>